# u-boot-update is a shell script which will source configuration files in this
# order (overwriting variables set by former scripts):
# 1. /etc/default/u-boot
# 2. /usr/share/u-boot-menu/conf.d/*.conf
# 3. /etc/u-boot-menu/conf.d/*.conf
#
# Thus, if you want to make changes to the Reform defaults found in
# /usr/share/u-boot-menu/conf.d/reform.conf you should put them into
# /etc/u-boot-menu/conf.d/reform.conf
#
# U_BOOT_ROOT and U_BOOT_PARAMETERS variables together form the "append" line
# in extlinux.conf like this:
#     append ${U_BOOT_ROOT} ${U_BOOT_PARAMETERS}"

# The U_BOOT_ROOT variable is meant to store the root=/dev/... parameter which
# is obtained as follows:
#  1. If both U_BOOT_PARAMETERS and U_BOOT_ROOT are empty, the root= parameter
#     is retrieved from /etc/kernel/cmdline.
#  2. If U_BOOT_ROOT is still empty, /etc/fstab is investigated for the "/"
#     entry.
#  3. If even that failed, the root= parameter is taken from /proc/cmdline
#
# Since we do not want to set the root= parameter (the initramfs takes care of
# figuring it out) but we also don't want this variable to have an effect, we
# set it to a single space which makes the variable not-empty, thus disabling
# above processing.
U_BOOT_ROOT=" "

# If not set, the U_BOOT_PARAMETERS variable by default gets set to "ro quiet".
#
# We rely on \${bootargs} (the backslash escapes the dollar sign with is a
# special shell character) to obtain the platform specific kernel parameters
# from u-boot.
#
# If you are not happy with the result, either append your custom options to
# the end of the U_BOOT_PARAMETERS string or take complete control over your
# cmdline by removing \${bootargs} from it so that you can ignore what u-boot
# tries to pass on.
#
# You can obtain the platform specific defaults by running this:
#
#     $ sh -c '. "/usr/share/reform-tools/machines/$(cat /proc/device-tree/model).conf"; echo $BOOTARGS'
#
U_BOOT_PARAMETERS="ro no_console_suspend cryptomgr.notests loglevel=3 \${bootargs} console=tty1"
